const fs = require('node:fs');
const table = require('table').table;
const readline = require('readline-sync');

// Handler para leer datos de entrada de instancia
function procesarPrincipal(datos) {
    let C = [];
    let lines = datos.split("\n");
    let coords = [];
    let readingCoods = false;

    for(let i in lines) {
        let line = lines[i];

        if(line.startsWith("NODE_COORD_SECTION")) {
            readingCoods = true;
        }
        else if(line.startsWith("EOF")) {
            readingCoods = false;

            let calculadorDistancia = function(x1, y1, x2, y2) {
                return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
            };

            for(let i = 0; i < coords.length; i++) {                
                for(let j = 0; j < coords.length; j++) {                    
                    if(i == j) {
                        C[i][j] = 0;
                    }
                    else {
                        let dist = calculadorDistancia (
                            coords[i][0],
                            coords[i][1],
                            coords[j][0],
                            coords[j][1]
                        );

                        C[i][j] = dist;
                        C[j][i] = dist;
                    }
                }
            }

            let tableOut = table(C);

            fs.writeFile("Cij.txt", tableOut, "utf8", function(err) {
                if(err) {
                    return console.log(err);
                }
            });
        }
        else if(readingCoods) {
            let coord = line.split(" ");
            coords.push([parseInt(coord[1]), parseInt(coord[2])]);
            C.push([[]])
        }
    }

    return C;
}

// Handler para leer archivo de ruta
function procesarDatosRuta(datos) {
    let lines = datos.split("\n");
    let ruta = new Map();
    let leyendoNodos = false;
    let nodoOrigen = -1;
    let nodoPrev = 0;

    for(let i in lines) {
        let line = lines[i];

        if(line.startsWith("TOUR_SECTION")) {
            leyendoNodos = true;
        }
        else if(line.startsWith("EOF")) {
            leyendoNodos = false;
        }
        else if(leyendoNodos) {
            let nNodo = parseInt(line);

            if(nodoOrigen == -1) {
                nodoOrigen = nNodo;
                nodoPrev = nNodo;
            }
            else if(nNodo == -1) {
                ruta.set(nodoPrev, nodoOrigen);
            }
            else {
                ruta.set(nodoPrev, nNodo);
                nodoPrev = nNodo;
            }
        }
    }

    return ruta;
}

// Función para calcular costo de ruta
function calcularCostoRuta(ruta, costos) {
    let costoTotal = 0;

    ruta.forEach((origen, destino) => 
        costoTotal += costos[origen - 1][destino - 1]
    );

    return costoTotal;
}

// Inicio ejecución
let datosInstancia;
let datosRuta;

let nArchivoInstancia = readline.question("Ingrese nonbre del archivo de datos de instancia (./att48.tsp):");

if(nArchivoInstancia == "")
    nArchivoInstancia = "./att48.tsp"

console.log("Leyendo datos de instancia...");

try {
    datosInstancia = fs.readFileSync(nArchivoInstancia, "utf-8");
} 
catch (error) {
    console.error("Error lectura archivo instancia:", error);
}

console.log("Generando matriz Cij...");
let Cij = procesarPrincipal(datosInstancia);
let nArchivoRuta = readline.question("Ingrese nonbre del archivo de datos de ruta (./att48.opt.tour):");

if(nArchivoRuta == "")
    nArchivoRuta = "./att48.opt.tour"

console.log("Leyendo datos de ruta...");

try {
    datosRuta = fs.readFileSync("./" + nArchivoRuta, "utf-8");
} 
catch (error) {
    console.error("Error lectura archivo ruta:", error);
}

console.log("Procesando ruta...");
let ruta = procesarDatosRuta(datosRuta);
let costoRuta = calcularCostoRuta(ruta, Cij);

console.log("El costo de la ruta es " + costoRuta);
console.log("Fin!");