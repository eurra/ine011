# Ejemplos INE001 - Métodos heuristicos para optimización

1) Vendedor Viajero (carpeta [tsp](tsp/))

Ejecutar archivo [main.js](tsp/main.js) para leer archivos de instancia y de ruta ejemplo (por defecto [att48.tsp](tsp/att48.tsp) y [att48.opt.tour](tsp/att48.opt.tour)), lo que permitirá calcular su costo respectivo.

2) Problema de la mochila 0/1 multidimensional (carpeta [mkp](mkp/))

Ver detalles en [README](mkp/README.md).