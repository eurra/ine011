import { readFileSync } from 'node:fs';

/*
    PASO 1: Lectura de archivo de configuración (JSON)

        Contiene tanto la configuración de la heurística como del dataset.
*/
let config;

try {
    config = JSON.parse(
        readFileSync('./config.json', { encoding: 'utf8', flag: 'r' })
    );
}
catch(err) {
    throw 'Cannot read config file: ' + err;
}

/*
    PASO 2: Inicialización de las estructuras de datos

        Parte de los datos vienen del archivo de configuración.
*/
// Esta función permite crear una objeto de solución vacía, con métodos para realizar manupulaciones básicas.
// La información de una solución será almacenada en las siguientes estructuras:
// a)   Un arreglo en donde cada posición guarda un contenedor en uso. A su vez, cada contenedor dentro del
//      arreglo será una estructura de tipo set (Set), que guardará los índices de los ítems asignados a dicho 
//      contenedor. Considerar que este elemento será estrictamente de uso interno, ya que la incorporación o
//      eliminación de contenedores se manejará internamente de forma automática, en base a asignación de ítems.
// b)   Un arreglo equivalente al definido en a), con la diferencia que guardará la capacidad actualmente ocupada
//      en cada contenedor.
// c)   Una estructura de tipo mapa (Map), que permitirá saber en que contenedor (índice del arreglo anterior) está
//      asignado un ítem específico. Para eso, las llaves del mapa serán los índices del item respectivo, y los 
//      valores el índice del contenedor al que se asigna.
// d)   Una estructura de tipo set (Set) que contendrá solo aquellos ítems NO asignados a la solución. En este sentido,
//      el conjunto de ítems en este arreglo será el complemento de las llaves del mapa anterior.
function createEmptySolution() {
    let bins = []                   // Estructura a)
    let usedCapacities = [];        // Estructura b)
    let setItems = new Map();       // Estructura c)
    let unsetItems = new Set();     // Estructura d)

    for(let i = 0; i < config.items.length; i++)
        unsetItems.add(i);

    return {
        // Retorna una copia de la solución
        copy() {
            let copy = createEmptySolution();

            for(let i in bins) {
                let binItems = bins[i];

                for(let item of binItems)
                    copy.setItem(item, i);
            }

            /*for(let [item, bin] of Object.entries(setItems))
                copy.setItem(item, bin);*/

            return copy;
        },
        // Retorna la calidad de la solucion, i.e., la cantidad de contenedores usados.
        getFitness() {
            return bins.length;
        },
        // Indica si la solución está completa (todos los ítems asignados)
        isComplete() {
            return unsetItems.size == 0;
        },
        // Obtiene el contenedor actualmente asignado para el item entregado.
        // Retorna el id (índice) del contenedor, o -1 si el ítem estaba disponible.
        getAssignedBinOf(item) {
            if(unsetItems.has(item))
                return -1;

            return setItems.get(item);  
        },
        // Asigna un ítem a un contenedor, sujeto a condiciones.
        // Retorna true o false según el resultado.
        setItem(item, bin) {
            // Se revisa que el item no esté seleccionado
            if(setItems.has(item))
                return false;

            // Se revisa que el contenedor exista en la solución. 
            if(bin > bins.length)
                return false;

            // Si se entrega como contenedor el número actual de ellos, significa que se 
            // insertará uno nuevo en la solución
            if(bin == bins.length) {
                bins.push(new Set());
                usedCapacities.push(0);
            }
            // En otro caso, se revisa que el contenedor tenga capacidad suficiente para 
            // incorporar el item
            else if(usedCapacities[bin] + config.items[item] > config.binCapacity)
                return false;

            // Chequeado todo esto, se agrega el item
            bins[bin].add(item);
            usedCapacities[bin] += config.items[item];            
            setItems.set(item, bin);
            unsetItems.delete(item);

            return true;
        },
        // Quita un item de su asignación actual
        // Retorna true o false según el resultado.
        unsetItem(item) {
            // Solo se debe verificar que el ítem este actualmente asignado
            if(!setItems.has(item))
                return false;

            let currBin = setItems.get(item);            
            bins[currBin].delete(item);
            usedCapacities[currBin] -= config.items[item];

            setItems.delete(item);
            unsetItems.add(item);

            // Si luego de eliminar el ítem, queda el contenedor vacío, corresponde quitarlo
            // Como el mapa setItems apunta a los índices de los contenedores, lo que se hace
            // es reemplazar el contenedor eliminado por el último de la solución (si es que 
            // hay), actualizando los ítems que estan en él
            if(bins[currBin].size == 0) {
                // Se realizará el movimiento mencionado solo si queda otro contenedor y el
                // que está siendo eliminado no es el de la última posición
                if(bins.length > 1 && currBin != bins.length - 1) {                    
                    bins[currBin] = bins[bins.length - 1];
                    usedCapacities[currBin] = usedCapacities[bins.length - 1];

                    for(let item of bins[bins.length - 1])
                        setItems.set(item, currBin);
                }

                // En cualquier escenario, se eliminan las últimas posiciones
                bins.pop();
                usedCapacities.pop();  
            }            

            return true;
        },
        // Retorna un arreglo con la informaciòn de los contenedores
        getBinsInfo() {
            let res = new Array();

            for(let bin in bins) {
                let binInfo = {};
                binInfo.id = bin;
                binInfo.usedCap = usedCapacities[bin];
                binInfo.items = Array.from(bins[bin]);

                res.push(binInfo);
            }

            return res;
        },
        // Retorna una copia de los ítems disponibles para ser incorporados en la solución, como Set
        getAvailableItems() {
            return new Set(unsetItems);
        },
        // Retorna una copia de los ítems asignados en la solución, como Set
        getAssignedItems() {
            return new Set(setItems.keys());
        }
    };
}

/*
    PASO 3: Definición de objeto con criterios de selección de ítems y contenedores

        Estos criterios permiten diversificar el funcionamiento de los operadores utilizados en la 
        heurística.

        Cada tipo de criterio recibe ciertos parámetros y debe retornar una estructura tipo iterador, 
        que permite ir iterando en el órden de los elementos a los cuales aplicó el criterio.
        El uso de estos criterios es definido en el archivo de configuración.
*/
// Objeto utilitario para generar un iterador de una lista entregada como entrada, y que se usará en los 
// operadores
function iterator(list) {
    let currIndex = 0;

    return {
        hasNext() {
            return currIndex < list.length;
        },
        next() {
            let ret = list[currIndex];
            currIndex++;

            return ret;
        },
        getAllNext() {
            let res = [];

            for(let i = currIndex; i < list.length; i++)
                res.push(list[i]);

            return res;
        }
    };
}

const criteria = {
    // En el caso de los criterios para selección de ítems, cada uno de ello recibe como argumento un
    // set de items a los cuales aplicará el criterio (tal como lo que entregan los métodos "getAvailableItems"
    // y "getAssignedItems" de un objeto solución), y entrega como resultado el objeto iterador que permitirá 
    // iterar los ítems en el orden establecido para el criterio.
    items: {
        // Criterio que genera un orden aleatorio de los ítems
        random(itemSet) {           
            let sourceList = Array.from(itemSet);
            let targetList = [];

            while(sourceList.length > 0) {
                let randomPos = Math.floor(Math.random() * sourceList.length);

                targetList.push(sourceList[randomPos]);
                sourceList.splice(randomPos, 1);
            }

            return iterator(targetList);
        },
        // Criterio que genera un orden de los ítems basado en su tamaño (de menor a mayor)
        size(itemSet) {
            let sourceList = Array.from(itemSet);
            let targetList = [];

            while(sourceList.length > 0) {
                let currSmallest = 0;
                let i = 1;

                while(i < sourceList.length) {
                    if(config.items[sourceList[i]] < config.items[currSmallest])
                        currSmallest = i;

                    i++;
                }

                targetList.push(sourceList[currSmallest]);
                sourceList.splice(currSmallest, 1);
            }

            return iterator(targetList);
        }
    },
    // En el caso de los criterios para selección de contenedores, cada uno de ello recibe como argumento un
    // un listado de objetos de información de contenedores (tal como lo que entrega el método "getBinsInfo" de
    // un objeto solución), y desde ello entrega como resultado el objeto iterador que permitirá iterar dichos
    // objetos en el orden establecido para el criterio.
    bins: {
        // Criterio que genera un orden aleatorio de los contenedores
        random(binSet) {
            let sourceList = Array.from(binSet);
            let targetList = [];

            while(sourceList.length > 0) {
                let randomPos = Math.floor(Math.random() * sourceList.length);

                targetList.push(sourceList[randomPos]);
                sourceList.splice(randomPos, 1);
            }

            return iterator(targetList);
        },
        // Criterio que genera un orden de los contenedores basado en su disponibilidad de espacio (de mayor a menor)
        availableSpace(binSet) {
            let sourceList = Array.from(binSet);
            let targetList = [];

            while(sourceList.length > 0) {
                let currMostSpace = 0;
                let i = 1;

                while(i < sourceList.length) {
                    if(sourceList[i].usedCap < sourceList[currMostSpace].usedCap)
                        currMostSpace = i;

                    i++;
                }

                targetList.push(sourceList[currMostSpace]);
                sourceList.splice(currMostSpace, 1);
            }

            return iterator(targetList);
        }
    }
};

/*
    PASO 4: Definición de objeto con operadores

        Cada operador es un método que recibe ciertos parámetros (siempre al menos la solución actual) y
        retorna una copia modificada de la solución, con el operador aplicado en ella.

        Los operadores son seleccionados en la heuristica con probabilidades que son configuradas en el
        archivo de configuración.
*/
const operators = {
    // Operador que asigna un ítem actualmente disponible a uno de los contenedores.
    // Tanto el ítem que será seleccionado como el contenedor en donde se asignará se definen a través de
    // los criterios entregados por parámetro.
    // Retorna la copia de la solución modificada, o null si hay problemas.
    assignItem(currentSol, itemCriteria, binCriteria) {
        let sortedItems = itemCriteria(currentSol.getAvailableItems());

        // Se chequea si es que quedan ítem disponibles
        if(!sortedItems.hasNext())
            return currentSol;

        let targetItem = sortedItems.next();        
        let resSol = currentSol.copy();
        let sortedBins = binCriteria(resSol.getBinsInfo());

        // Se chequea si hay disponibilidad en los actuales contenedores y de acuerdo al criterio entregado.
        // Si no fuese así, se incorpora un nuevo contenedor a la solución
        let added = false;

        while(!added && sortedBins.hasNext())
            added = resSol.setItem(targetItem, sortedBins.next().id)

        if(!added) {
            let targetBin = resSol.getFitness();
            resSol.setItem(targetItem, targetBin);
        } 
         
        return resSol;
    },
    // Operador que reasigna un ítem actualmente asignado a uno de los contenedores
    // Tanto el ítem que será reasignado como el contenedor en donde se moverá se definen a través de
    // los criterios entregados por parámetro.
    // Retorna la copia de la solución modificada, o null si hay problemas.
    relocateItem(currentSol, itemCriteria, binCriteria) {
        let sortedItems = itemCriteria(currentSol.getAssignedItems());

        // Se chequea si hay ítems seleccionados
        if(!sortedItems.hasNext())
            return currentSol;

        // Se elimina el item desde una copia de la solución
        let targetItem = sortedItems.next();
        let resSol = currentSol.copy();
        resSol.unsetItem(targetItem);

        // Se aplica el operador de asignación
        return operators.assignItem(resSol, itemCriteria, binCriteria);

        /*
        // Se aplica el criterio para seleccionar el contenedor donde se moverá el ítem
        let binsSorted = binCriteria(resSol.getBinsInfo());        

        // Se chequea si hay disponibilidad en los actuales contenedores y de acuerdo al criterio entregado.
        // Si no fuese así, se incorpora un nuevo contenedor a la solución.
        let targetBin;

        if(!binsSorted.hasNext())
            targetBin = resSol.getFitness();
        else 
            targetBin = binsSorted.next().id;

        
        resSol.setItem(targetItem, targetBin);
        return resSol;*/
    },
    // Realiza un proceso iterativo para llenar múltiples contenedores con los ítems disponibles en la solución.
    // Los ítems se van seleccionando iterativamente según el criterio entregado como parámetro, y se va 
    // evaluando si es posible incorporarlos en los contenedores según el orden del criterio aplicado para 
    // ellos. Cuando un ítem logra ser efectivamente incorporado a un contenedor, el ítem siguiente es
    // seleccionado, y se vuelven a probar todos los contenedores nuevamente para dicho ítem, según el 
    // criterio respectivo.
    fillBins(currentSol, itemCriteria, binCriteria) {        
        let itemIterator = itemCriteria(currentSol.getAvailableItems()); 

        // Se chequea si es que quedan ítem disponibles
        if(!itemIterator.hasNext())
            return currentSol;        
        
        let resSol = currentSol.copy();

        // Se inicia el proceso iterativo, mientras no queden ítems por asignar
        while(itemIterator.hasNext()) {
            let targetItem = itemIterator.next();

            // Se genera el listado ordenado (acorde al criterio) de contenedores (actualizado cada vez que se
            // agrega un item)
            let sortedBins = binCriteria(resSol.getBinsInfo()).getAllNext();

            // Se va probando en cada contenedor, en el orden establecido por el criterio, viendo si es posible
            // incorporar el item en él
            let i = 0;

            while(i < sortedBins.length && !resSol.setItem(targetItem, sortedBins[i].id))
                i++;

            // Si se alcanzó el final del listado de contenedores, significa que no se pudo incorporar el ítem en
            // ninguno de ellos, entonces se procede a agregar un nuevo contenedor a la solución y a incorporar el
            // ítem en él
            if(i == sortedBins.length) {
                let newBin = resSol.getFitness();
                resSol.setItem(targetItem, newBin);
            }
        }

        // Finalmente, se entrega el resultado
        return resSol;
    },
    // Selecciona un contenedor acorde al criterio entregado por parámetro, y lo vacía completamente, deseleccionando
    // sus ítems asignados.
    emptyBin(currentSol, itemCriteria, binCriteria) {
        let targetBinIt = binCriteria(currentSol.getBinsInfo());

        // Se revisa si no hay contenedores        
        if(!targetBinIt.hasNext())
            return currentSol;

        let targetBin = targetBinIt.next();
        let copySol = currentSol.copy();

        for(let i in targetBin.items)
            copySol.unsetItem(targetBin.items[i]);

        return copySol;
    },
    // Ejecuta el vaciado de un contenedor y el llenado de items de forma secuencial
    emptyBinAndRefill(currentSol, itemCriteria, binCriteria) {
        let newSol = operators.emptyBin(currentSol, itemCriteria, binCriteria);
        return operators.fillBins(newSol, itemCriteria, binCriteria);
    }
};

/*
    PASO 5: Implementación de la heurística

        Acá se especifica el código que se ejecuta para la ejecución de la heurística, utilizando todos los insumos 
        definidos previamente.
*/
function heuristic() {
    // Inicialización de variables de control
    let iteration = 1;
    let lastTimestamp = Date.now();
    let elapsedTime = 0;

    // Definición de rangos de probabilidad para los operadores, acorde al archivo de configuración
    let ops = [];
    let randomRanges = [];
    let currRange = 0.0;

    for(let [name, op] of Object.entries(operators)) {
        if(config.operatorSelectionWeights[name] == 0.0)
            continue;

        ops.push(op);
        currRange += config.operatorSelectionWeights[name];
        randomRanges.push(currRange);
    }

    // Generación de solución inicial
    let currSolution = createEmptySolution();

    for(let i = 0; i < config.items.length; i++)
        currSolution.setItem(i, currSolution.getFitness())

    let bestSolution = currSolution;

    console.log("\n*********************************************************\n");
    console.log(`Solución inicial (fitness: ${bestSolution.getFitness()}):`);
    console.log(bestSolution.getBinsInfo());
    console.log("\n*********************************************************\n");

    // Aplicación de bucle principal
    while(iteration < config.maxIterations && Math.floor(elapsedTime / 1000) < config.maxSeconds) {
        // Se lanza un número aleatorio para ver la aplicación de los operadores
        let random = Math.random();
        let opSelected = -1;
        let i = 0;

        while(i < randomRanges.length && opSelected == -1) {
            if(i == randomRanges.length - 1 || (random <= randomRanges[i]))
                opSelected = i;

            i++;
        }

        // Se aplica el operador seleccionado        
        currSolution = ops[opSelected](
            currSolution, 
            criteria.items[config.itemSelectCriteria],
            criteria.bins[config.binSelectCriteria]
        );

        // Se reporta una mejor solución, si es que está completa y se mejora la calidad
        if(currSolution.isComplete() && currSolution.getFitness() < bestSolution.getFitness()) {
            bestSolution = currSolution;

            console.log(`Se encontró una mejor solución (fitness: ${bestSolution.getFitness()}):`);
            console.log(bestSolution.getBinsInfo(), "\n");
        }

        // Se actualizan los datos de control
        iteration++;

        if(iteration % (config.maxIterations / 10) == 0)
            console.log(`Iteración: ${iteration}`);

        elapsedTime += Date.now() - lastTimestamp;
        lastTimestamp = Date.now();
    }

    // Terminado el proceso, se reportan los resultados
    console.log("\n*********************************************************\n");
    console.log(`Mejor solución encontrada (fitness: ${bestSolution.getFitness()}):`);
    console.log(bestSolution.getBinsInfo());
    console.log(`Tiempo de ejecución: ${Math.floor(elapsedTime / 1000)} seg.`);
    console.log(`Total de iteraciones: ${iteration}`);
    console.log("\n*********************************************************\n");
}

/*
    PASO 6: Se ejecuta!
*/
heuristic();