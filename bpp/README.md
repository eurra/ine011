# Bin Packing Problem (MKP)

En este problema se tiene un conjunto finito de contenedores con una capacidad fija cada uno, en los cuales se deben asignar distintos ítems de diferentes tamaños cada uno. El objetivo del problema es minimizar la cantidad de contenedores que son utilizados como solución. La definición formal (matemática) del problema es como sigue a continuación:

1) Variables

Sea $I$ el conjunto finito de ítems a asignar, en donde cada ítem $i \in I$ tiene un tamaño $s(i) \in \mathbb{Z}^+$. También se tiene un conjunto de $n$ contenedores, donde cada uno tiene una capacidad fija $B$. En el problema se definen las siguientes variables de decisión:

```math
(1)\quad y_j \in \{0,1\} \quad\forall j \in \{1,...,n\}
```
```math
(2)\quad x_{ij} \in \{0,1\} \quad\forall i \in I, \forall j \in \{1,...,n\}
```

La variable 1 indica si el contenedor $j$ es seleccionado o en la solución, mientras que la variable 2 indica que el item $i$ es asignado al contenedor $j$.

2) Función objetivo:

El problema consiste en optimizar el valor $K$ tal que:

```math
(3)\quad K = \sum_{j = 1}^{n}{y_j}
```

donde $K$ representa el número total de contenedores utilizados en la solución, para los cuales ${y_j = 1}$.

3) Restricciones

```math
(4)\quad K \geq 1
```
```math
(5)\quad \sum_{i \in I}{s(i) x_{ij}} \leq B y_j, \quad \forall j \in \{1,...,n\}
```
```math
(6)\quad \sum_{j = 1}^{n}{x_{ij}} = 1, \quad \forall i \in I    
```

La restricción 4 indica que una solución factible incluye al menos un contenedor.

La restricción 5 indica que la suma de los tamaños $s(i)$ de todos los items $x_{ij}$ que están asignados a un contenedor $y_j$ sea menor o igual a la capacidad $B$, esto cuando el contenedor $j$ este incluído en la solución (${y_j = 1}$).

Finalmente, la restricción 6 indica que cada uno de los ítems deben estar asignados a un solo contenedor, es decir, que la suma de todas las variables $x_{ij}$ para un $i$ específico debe ser 1.

# Heuristica implementada

El código del archivo [main.mjs](main.mjs) implementa los siguientes componentes:

* Lectura de un archivo de configuración ([config.json](config.json)) con parámetros tanto del problema como de la heurística.
```javascript
let config;

try {
    config = JSON.parse(
        readFileSync('./config.json', { encoding: 'utf8', flag: 'r' })
    );
}
catch(err) {
    throw 'Cannot read config file: ' + err;
}
```

```json
// config.json
{
    "maxIterations": 10000000,
    "maxSeconds": 20,
    "binCapacity": 100,
    "items": [ 49, 41, 34, 33, 29, 26, 26, 22, 20, 19 ],
    "itemSelectCriteria": "...",
    "binSelectCriteria": "...",
    "operatorSelectionWeights": { 
        // ...
    }
}
```

* Estructura de datos asociadas a la solución (se explican en el código):

    La función `createEmptySolution()` crea una solución vacía, y a través de diversos métodos como `setItem(item, bin)` y `unsetItem(item)` modifican las estructuras definidas:

```javascript
function createEmptySolution() {
    let bins = []
    let usedCapacities = []; 
    let setItems = new Map();
    let unsetItems = new Set(); 

    // ...

    return {
        // ...
        setItem(item, bin) {
            //...
        },
        unsetItem(item) {
            // ...
        }
    // ...
```
* Un objeto con las definiciones de **criterios** a través de los cuales los operadores a implementar seleccionan tanto ítems como contenedores para hacer modificaciones a una solución. Por ahora se implementan dos criterios `random` y `size` para ítems, junto a `random` y `availableSpace` para contenedores

```javascript
const criteria = {
    items: {
        random(itemSet) { 
            // ...
        },        
        size(itemSet) {
            // ...
        }
    },
    bins: {
        random(binSet) { 
            // ...
        },        
        availableSpace(binSet) {
            // ...
        }
    }
}
```

* Un objeto con los **operadores** que se pueden configurar en el archivo [config.json](config.json) y que se usarán en la heurística. Se incluyen 5 distintos (explicados en el código):

```javascript
const operators = {
    assignItem(/* ... */) { /* ... */ },
    relocateItem(/* ... */) { /* ... */ },
    fillBins(/* ... */) { /* ... */ },
    emptyBin(/* ... */) { /* ... */ },
    emptyBinAndRefill(/* ... */) { /* ... */ }
}
```

* Finalmente, la **heurística** propiamente tal, que se ejecuta al final del archivo:

```javascript
function heuristic() {
    // ...
}

//...
heuristic();
```